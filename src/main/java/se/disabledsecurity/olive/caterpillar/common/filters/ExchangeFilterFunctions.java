package se.disabledsecurity.olive.caterpillar.common.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import reactor.core.publisher.Mono;

public class ExchangeFilterFunctions {
	public static final Logger LOGGER = LoggerFactory.getLogger(ExchangeFilterFunctions.class);

	private static final String NEW_LINE = System.getProperty("line.separator");
	private ExchangeFilterFunctions() {}

	public static ExchangeFilterFunction logRequest() {
		return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
			StringBuffer buffer = new StringBuffer();

			return Mono.just(clientRequest);
		});
	}

	private static void logHeaders(ClientRequest request) {
		request.headers().forEach((name, values) -> values.forEach(value -> logNameAndValuePair(name, value)));
	}
	private static void logNameAndValuePair(String name, String value) {
		LOGGER.debug("{}={}", name, value);
	}

}
