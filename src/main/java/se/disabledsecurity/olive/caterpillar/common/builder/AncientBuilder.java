package se.disabledsecurity.olive.caterpillar.common.builder;

public interface AncientBuilder<T, U> extends Builder<U>
{
    T self();
}
