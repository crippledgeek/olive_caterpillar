package se.disabledsecurity.olive.caterpillar.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URL;

/**
 * This class is used to map the properties under the 'alerts' prefix in the application properties file.
 * It uses the Spring Boot @ConfigurationProperties for binding the properties.
 */
@Data
@ConfigurationProperties(prefix = "alerts")
public class AlertProperties {
    /**
     * The base URL for the alerts.
     */
    private URL baseUrl;
    /**
     * The scheduler properties for the alerts.
     */
    private Scheduler scheduler;

    /**
     * This class is used to map the scheduler properties for alerts.
     */
    @Data
    public static class Scheduler {

        /**
         * The initial delay (in minutes) before the scheduler runs for the first time.
         */
        private int initialDelayInMinutes;
        /**
         * The interval (in minutes) between each run of the scheduler.
         */
        private int intervalInMinutes;
    }
}