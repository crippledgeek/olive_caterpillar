package se.disabledsecurity.olive.caterpillar.common.builder;

public interface ReplicaBuilder<T extends Builder<T>> {

    T replicaBuilder();
}



