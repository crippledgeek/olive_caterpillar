package se.disabledsecurity.olive.caterpillar.common.builder;


/**
 * The interface Builder.
 *
 * @param <T> the type parameter
 */
public interface Builder<T> {
    /**
     * Build T.
     *
     * @return the T
     */
    T build();
}
