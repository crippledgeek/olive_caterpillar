package se.disabledsecurity.olive.caterpillar.common.builder;

public interface AncientReplicaBuilder<Builder extends se.disabledsecurity.olive.caterpillar.common.builder.Builder<?>> {

    Builder replicaBuilder(Builder builder);
}


