package se.disabledsecurity.olive.caterpillar.internal.model;
import lombok.Builder;

@Builder(setterPrefix = "with")
public record Location(String name, String gps) {}