package se.disabledsecurity.olive.caterpillar.internal.model;

import lombok.Builder;

import java.time.ZonedDateTime;
@Builder(setterPrefix = "with")
public record Alert(String summary, ZonedDateTime datetime, String name, Location location, Integer id, EventType type, String url) {}
