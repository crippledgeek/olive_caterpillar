package se.disabledsecurity.olive.caterpillar.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;
import se.disabledsecurity.olive.caterpillar.internal.model.Alert;
import se.disabledsecurity.olive.caterpillar.mapper.AlertMapper;
import se.disabledsecurity.olive.caterpillar.service.AlertApi;

@Service
public class AlertJob implements Job {

    private final Sinks.Many<Message<Alert>> messageBuffer;
    private final AlertApi alertService;

    public AlertJob(Sinks.Many<Message<Alert>> messageBuffer, AlertApi alertService) {
        this.messageBuffer = messageBuffer;
        this.alertService = alertService;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        alertService.getAll()
                .log()
                .subscribeOn(Schedulers.boundedElastic())
                .mapNotNull(AlertMapper.map)
                .subscribe(messageBuffer::tryEmitNext);
    }
}
