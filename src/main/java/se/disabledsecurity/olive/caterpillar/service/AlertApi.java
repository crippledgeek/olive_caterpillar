package se.disabledsecurity.olive.caterpillar.service;

import reactor.core.publisher.Flux;
import se.disabledsecurity.olive.caterpillar.external.model.AlertItem;

import java.util.List;

public interface AlertApi {
	Flux<AlertItem> getAll();
}
