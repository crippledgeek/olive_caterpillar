package se.disabledsecurity.olive.caterpillar.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import se.disabledsecurity.olive.caterpillar.external.model.AlertItem;
@Service
public class AlertService implements AlertApi {

	private final SwedishPoliceAuthorityApiService policeAuthorityApiService;

	public AlertService(SwedishPoliceAuthorityApiService policeAuthorityApiService) {
		this.policeAuthorityApiService = policeAuthorityApiService;
	}

	@Override
	public Flux<AlertItem> getAll() {
		return policeAuthorityApiService.getAll();

	}
}
