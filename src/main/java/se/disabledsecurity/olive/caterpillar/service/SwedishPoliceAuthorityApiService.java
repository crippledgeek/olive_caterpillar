package se.disabledsecurity.olive.caterpillar.service;

import org.springframework.web.service.annotation.GetExchange;
import reactor.core.publisher.Flux;
import se.disabledsecurity.olive.caterpillar.external.model.AlertItem;

public interface SwedishPoliceAuthorityApiService {
	@GetExchange("/events")
	Flux<AlertItem> getAll();


}
