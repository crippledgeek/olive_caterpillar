package se.disabledsecurity.olive.caterpillar.mapper;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import se.disabledsecurity.olive.caterpillar.external.model.AlertItem;
import se.disabledsecurity.olive.caterpillar.internal.model.Alert;
import se.disabledsecurity.olive.caterpillar.internal.model.EventType;
import se.disabledsecurity.olive.caterpillar.internal.model.Location;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class AlertMapper {

	private AlertMapper() {
	}

	private static final Supplier<String> SWEDISH_POLICE_AUTHORITY_BASE_URL = ()-> "http://polisen.se";
	private static final Function<Alert, Message<Alert>> toMessage = alert -> MessageBuilder.withPayload(alert).build();
	private static final Function<se.disabledsecurity.olive.caterpillar.external.model.Location, Location> toLocation = location -> Location.builder()
																																			.withGps(location.gps())
																																			.withName(location.name())
																																			.build();
	private static final Function<se.disabledsecurity.olive.caterpillar.external.model.EventType, EventType> toEvent = eventType -> EventType.valueOf(eventType.name());

	private static final BiFunction<String, String, String> toEventUrl = String::concat;

	private static final Function<AlertItem, Alert> toAlert = alertItem -> Alert.builder()
												  .withId(alertItem.id())
												  .withDatetime(alertItem.datetime())
												  .withName(alertItem.name())
												  .withUrl(toEventUrl.apply(SWEDISH_POLICE_AUTHORITY_BASE_URL.get(), alertItem.url()))
												  .withSummary(alertItem.summary())
												  .withLocation(toLocation.apply(alertItem.location()))
												  .withType(toEvent.apply(alertItem.type()))
												  .build();
	public static final Function<AlertItem, Message<Alert>> map = alertItem -> toAlert.andThen(toMessage).apply(alertItem);

}
