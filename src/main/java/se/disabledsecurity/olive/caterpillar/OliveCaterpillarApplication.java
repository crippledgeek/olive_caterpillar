package se.disabledsecurity.olive.caterpillar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import se.disabledsecurity.olive.caterpillar.common.properties.AlertProperties;

@SpringBootApplication
@EnableConfigurationProperties(AlertProperties.class)
public class OliveCaterpillarApplication {
    public static void main(String[] args) {
        SpringApplication.run(OliveCaterpillarApplication.class, args);
    }
}
