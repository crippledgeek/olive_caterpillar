package se.disabledsecurity.olive.caterpillar.configuration;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.disabledsecurity.olive.caterpillar.common.properties.AlertProperties;
import se.disabledsecurity.olive.caterpillar.jobs.AlertJob;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

@Configuration
public class SchedulerConfiguration {

    private final AlertProperties alertProperties;

    public SchedulerConfiguration(AlertProperties alertProperties) {
        this.alertProperties = alertProperties;
    }

    @Bean
    public JobDetail jobADetails() {
        return JobBuilder
                .newJob(AlertJob.class)
                .withIdentity("getAlertsJob")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger jobATrigger(JobDetail jobADetails) {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(AlertJob.class.getSimpleName())
                .startAt(Date.from(Instant.now().plus(alertProperties.getScheduler().getInitialDelayInMinutes(), ChronoUnit.MINUTES)))
                .forJob(jobADetails)
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(alertProperties.getScheduler().getIntervalInMinutes())
                        .repeatForever()
                        .build()
                        .getScheduleBuilder())
                .build();
    }

}
