package se.disabledsecurity.olive.caterpillar.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import se.disabledsecurity.olive.caterpillar.internal.model.Alert;

import java.util.function.Supplier;

@Configuration
public class AlertSource {

    @Bean
    public Supplier<Flux<Message<Alert>>> alerts() {
        return messageBuffer()::asFlux;
    }

    @Bean
    public Sinks.Many<Message<Alert>> messageBuffer() {
        return Sinks.many().multicast().onBackpressureBuffer();
    }


}
