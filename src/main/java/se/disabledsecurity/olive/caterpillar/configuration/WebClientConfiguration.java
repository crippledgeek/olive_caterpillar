package se.disabledsecurity.olive.caterpillar.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;
import se.disabledsecurity.olive.caterpillar.common.properties.AlertProperties;
import se.disabledsecurity.olive.caterpillar.service.SwedishPoliceAuthorityApiService;

@Configuration(proxyBeanMethods = false)
public class WebClientConfiguration {

    private final AlertProperties alertProperties;

    public WebClientConfiguration(AlertProperties alertProperties) {
        this.alertProperties = alertProperties;
    }

    @Bean
    public WebClient alertClint(WebClient.Builder builder) {
        return builder
                .baseUrl(alertProperties.getBaseUrl().toString())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }
    @Bean
    SwedishPoliceAuthorityApiService policeAuthorityApiService(WebClient webClient) {
        HttpServiceProxyFactory httpServiceProxyFactory =
                HttpServiceProxyFactory
                        .builder()
                        .exchangeAdapter(WebClientAdapter.create(webClient))
                        .build();
        return httpServiceProxyFactory.createClient(SwedishPoliceAuthorityApiService.class);
    }
}
