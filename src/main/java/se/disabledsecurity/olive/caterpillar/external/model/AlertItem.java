package se.disabledsecurity.olive.caterpillar.external.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import se.disabledsecurity.olive.caterpillar.external.deserializers.PoliceEventApiDateDeserializer;

import java.time.ZonedDateTime;


public record AlertItem(@JsonProperty("summary") String summary,
                        @JsonProperty("datetime") @JsonDeserialize(using = PoliceEventApiDateDeserializer.class) ZonedDateTime datetime,
                        @JsonProperty("name") String name,
                        @JsonProperty("location") Location location,
                        @JsonProperty("id") Integer id,
                        @JsonProperty("type") EventType type,
                        @JsonProperty("url") String url) {}
