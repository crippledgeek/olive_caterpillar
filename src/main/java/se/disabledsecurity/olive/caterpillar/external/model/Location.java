package se.disabledsecurity.olive.caterpillar.external.model;

import lombok.Builder;

@Builder
public record Location(String name, String gps) {
}
