package se.disabledsecurity.olive.caterpillar.external.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@JsonComponent
public class PoliceEventApiDateDeserializer extends JsonDeserializer<ZonedDateTime> {

    @Override
    public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return jsonParser
                .getCodec()
                .readValue(jsonParser, String.class)
                .lines()
                .map(s -> s.split(" "))
                .map(strings -> ZonedDateTime.of(LocalDate.parse(strings[0]), mapTime(strings[1]), ZoneId.ofOffset("GMT", ZoneOffset.of(strings[2]))))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private static LocalTime mapTime(final String timeString) {
        return LocalTime.parse(timeString.length() == 7 ? "0%s".formatted(timeString) : timeString);
    }
}
