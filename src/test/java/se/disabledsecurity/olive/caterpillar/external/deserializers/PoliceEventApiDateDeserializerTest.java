package se.disabledsecurity.olive.caterpillar.external.deserializers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.core.io.ResourceLoader;
import se.disabledsecurity.olive.caterpillar.internal.model.Alert;

import java.io.IOException;
import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@JsonTest
class PoliceEventApiDateDeserializerTest {

    @Autowired
    private final ResourceLoader loader = null;
    @Autowired
    private JacksonTester<Alert> json;

    @Test
    void deserialize() throws IOException {
        byte[] bytes = loader.getResource("classpath:e.json").getInputStream().readAllBytes();
        Alert object = json.parse(bytes).getObject();
        assertThat(object.datetime(), equalTo(ZonedDateTime.parse("2021-10-10T07:00:24+02:00[GMT+02:00]")));
    }
}
